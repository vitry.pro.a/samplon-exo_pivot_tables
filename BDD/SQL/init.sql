DROP DATABASE IF EXISTS pivot_table;
CREATE DATABASE IF NOT EXISTS pivot_table;

USE pivot_table;

CREATE TABLE `product` (
  `id_product` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL UNIQUE
);

CREATE TABLE `country` (
  `id_country` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL UNIQUE
);

CREATE TABLE `product_country` (
  `id_order` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `id_product` int NOT NULL,
  `id_country` int NOT NULL,
  `category` ENUM ('Vegetables', 'Fruit') NOT NULL,
  `amount` int NOT NULL,
  `date` date
);

CREATE UNIQUE INDEX `product_country_index_0` ON `product_country` (`id_product`, `id_country`, `category`, `amount`, `date`);

ALTER TABLE `product_country` ADD FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`);

ALTER TABLE `product_country` ADD FOREIGN KEY (`id_country`) REFERENCES `country` (`id_country`);