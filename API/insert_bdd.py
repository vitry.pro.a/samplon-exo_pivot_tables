from extract_file import extract_data_excel
from utile_mysql import request_bdd_wtih_condition, add_1_value, last_id, insert_bdd_1_value, insert_bdd_all_value
import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="antony",
    password="choupette",
    database="pivot_table"
)

mycursor = mydb.cursor(buffered=True)

def formatage_df(dataframe):
    # Etape 0 : Fomatage des colonnes
    dataframe.rename(columns=str.lower, inplace=True)
    dataframe.rename(columns={'order id': 'id_order'}, inplace=True)
        # ['order id', 'product', 'category', 'amount', 'date', 'country']
    # Etape 1 : Fomatage de l'index
    dataframe.set_index("id_order", inplace=True)
    # Etape 2: convertir le format de la date
    dataframe['date'] = dataframe['date'].dt.strftime('%Y-%m-%d') # AAAA-MM-JJ
    # Etape 3: convertir les strings en lowercase
    dataframe['product'] = dataframe['product'].str.lower()
    dataframe['category'] = dataframe['category'].str.lower()
    dataframe['country'] = dataframe['country'].str.lower()
    # Etape 4: convertir amouts en int
    dataframe["amount"] = dataframe["amount"].astype(str).astype(int)
    return dataframe


def insertion_data(dataframe):
    # {'product': 0, 'category': 1, 'amount': 2, 'date': 3, 'country': 4}
    list_table = ["product", "country", "product_country"]
    list_column_product = ["id_product", "name"]
    list_column_country = ["id_country", "name"]
    list_column_product_country = ["id_order", "id_product", "id_country", "category", "amount", "date"]
    for index in range(len(dataframe)):
        print("-" * 25, "Insertion Data:", "-" * 25)
        # Insertion country
        value_country = dataframe.iloc[index, 4]
        print("Row: ", index, " / Country: ", value_country)
        # Valeur dans fonction:
        # print(list_table[1], list_column_country[1], list_column_country[1])
        id_country = insert_bdd_1_value(mydb, mycursor, list_table[1], list_column_country[1], list_column_country[1], value_country)
        mydb.commit()
        print("Id_country: ", id_country)
        print()
        # Insertion product
        value_product = dataframe.iloc[index, 0]
        print("Row: ", index, " / Product: ", value_product)
        # Valeur dans fonction:
        # print(list_table[0], list_column_product[1], list_column_product[1])
        id_product = insert_bdd_1_value(mydb, mycursor, list_table[0], list_column_product[1], list_column_product[1], value_product)
        mydb.commit()
        print("Id_product: ", id_product)
        print()
        # Insertion product_country
        list_column_condition = list_column_product_country[1:]
        list_value = [id_product, id_country, dataframe.iloc[index, 1], int(dataframe.iloc[index, 2]), dataframe.iloc[index, 3]]
        print("Row:", index, "/ list_column_condition:", list_column_condition)
        print("Champs:", list_value)
        # Valeur dans fonction: ["product_country", [list_column_product_country], [list_column_condition], list_value]
            # table: str, column: list, column_condition: list, value_condition: list
        id_order = insert_bdd_all_value(mydb, mycursor, list_table[2], list_column_product_country[1:], list_column_condition, list_value)
        mydb.commit()
        print("Id_order: ", id_order)
        print()


if __name__=="__main__":
    df_unformated = extract_data_excel(2)
    df = formatage_df(df_unformated)
    print(df)
    insertion_data(df)



