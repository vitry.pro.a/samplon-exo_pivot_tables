import pandas as pd

def extract_data_excel(sheet=2):
    df = pd.read_excel('DATA/pivot-tables.xlsx', sheet_name=sheet)
    return df


if __name__=="__main__":
    df_index_1 = extract_data_excel(0)
    print(df_index_1)
    df_index_2 = extract_data_excel(1)
    print(df_index_2)
    df_index_3 = extract_data_excel(2)
    print(df_index_3)
    df_index_default = extract_data_excel()
    print(df_index_default)