# Analyse du taleau Excel

## Les noms de colonne:

- Order ID -> Identifiant de la ligne
Colonne à reporter dans la BDD sous forme order_id

- Product -> nom du produit
Liste de produits différents
Colonne à reporter en tant que Table Product

- Category -> nom de la catégorie du produit
Catégorie du produit: caractéristique du produit
Colonne à reporter comme un Enum dans la BDD

- Amount -> montant du produit
Prix du produit: caractéristique du produit

- Date -> date du produit
Date de transaction du produit: caractéristique du produit

- Country -> pays du produit
Liste de pays en fonction du produit
Colonne à reporter en tant que Table Country

![MCD](/BDD/MLD/pivot_tables_schema.png "MCD")

## Modélisation de la structure de la BDD

- Table: product
    Caractéristique:
    - id_product : int
    - name : str
- Table: country
    Caractéristique:
    - id_country : int
    - name : str
- Table de relation: product_country
    Caractéristique:
    - id_product : int
    - id_country : int
    - category : enum (Vegetables, Fruit)
    - amount : int
    - date : date

![MCD](/BDD/MLD/Pivot_tables.png "MCD")