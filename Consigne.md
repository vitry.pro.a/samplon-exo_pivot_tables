Un classeur excel. 
- des données brutes
- 2 TCD.
Ce qu'il faut faire :
- Analyser le fichier
- Modeliser une BDD pour recupérer les données du fichier. + Etablir les règles de normalisation des données.
- Mettre les données du fichier et les mettres dans la bdd. En les normalisant.
- Avec pandas faire des tables équivalentes aux TCD du fichier.
- Faire des fonctions en python qui permettent de récupérer les tables pandas.
Option:
- faire une api qui permet d'appeler les fonctions.

+ mettre le résultat de votre travail sur un repo GIT.
+ rendre le travail pour mardi 27/02/2024 à 9h00 
